#ifndef __OPENGROUPS_H
#define __OPENGROUPS_H

#include <vdr/tools.h>


class cMultiOption : public cListObject {
private:
  char *value;
public:
  cMultiOption(void);
  virtual ~cMultiOption();
  char *Value(void)          { return value; };
  void SetValue(char *Value) { value = strdup(Value); };
  };

class cMultiList : public cList<cMultiOption> {
public:
  cMultiList(void);
//  virtual ~cMultiList(); // valgrind
  ~cMultiList();
  void Dump(void);
  cMultiList *Copy(void);
  void AddValue(char *newValue);
  void AppendList(cMultiList *ListToAppend);
  char *DeleteDoubles(void);
  };

class cOpenGroups {
private:
  cMultiList *openCa;
  cMultiList *closedCa;
  cMultiList *openProvider;
  cMultiList *closedProvider;
public:
  cOpenGroups(void);
  virtual ~cOpenGroups();
  bool Load(void);
  bool ParseOption(char *option);
  void Dump(void);
  void Clear(void);
  cMultiList *OpenCa(void)          { return openCa; };
  cMultiList *ClosedCa(void)        { return closedCa; };
  cMultiList *OpenProvider(void)    { return openProvider; };
  cMultiList *ClosedProvider(void)  { return closedProvider; };
};

extern cOpenGroups OpenGroups;


#endif
