#include "opengroups.h"
#include <vdr/plugin.h>
#include "autosort_tools.h"
#include "autosort_menu.h"
#include <string.h>

cOpenGroups OpenGroups;

cOpenGroups::cOpenGroups(void)
{
  openCa         = new cMultiList;
  closedCa       = new cMultiList;
  openProvider   = new cMultiList;
  closedProvider = new cMultiList;
}

cOpenGroups::~cOpenGroups()
{
//  dsyslog("Destructor cOpenGroups");
  delete openCa;
  delete closedCa;
  delete openProvider;
  delete closedProvider;
}

void cOpenGroups::Clear(void)
{
//  dsyslog("cOpenGroups Clear()");
  openCa->Clear();
  closedCa->Clear();
  openProvider->Clear();
  closedProvider->Clear();
}

//ToDo: Change this
//refer HISTORY 1.3.36
#define MAXPARSEBUFFER KILOBYTE(10)

bool cOpenGroups::Load(void)
{
  char *buf;
  char *option;
//  asprintf(&buf, "%s/%s/%s",cPlugin::ConfigDirectory(),"autosort","opengroups.conf" );
  asprintf(&buf, "%s/%s/%s",ConfigDir,"autosort","opengroups.conf" );
    if (access(buf, R_OK) == 0) {
      dsyslog ("loading %s", buf);
      FILE *f = fopen(buf, "r");
      if (f) {
        char buffer[MAXPARSEBUFFER];
        while (fgets(buffer, sizeof(buffer), f) > 0) {
           char *p = strchr(buffer, '#');
           if (p) continue;
	  stripspace(buffer);
          if (!isempty(buffer)) {
            option = strtok(buffer,":");
	    ParseOption(option);
            }
          }
        fclose(f);
        }
      free (buf); // valgrind
      return true;
      } else {
        dsyslog ("no file: %s ", buf);
        free (buf); // valgrind
	return false;
	}
}


bool cOpenGroups::ParseOption(char *option)
{
    if (!strncasecmp(option, "OpenCa",6))  { 
      strsep(&option,"=");
      cMultiOption *multi = new cMultiOption;
      multi->SetValue(UpString(option));
      openCa->Add(multi);
      }						      
    else if (!strncasecmp(option, "ClosedCa",8))  { 
      strsep(&option,"=");
      cMultiOption *multi = new cMultiOption;
      multi->SetValue(UpString(option));
      closedCa->Add(multi);
      }
    else if (!strncasecmp(option, "OpenProvider",12))  { 
      strsep(&option,"=");
      cMultiOption *multi = new cMultiOption;
      multi->SetValue(UpString(option));
      openProvider->Add(multi);
      }						      
    else if (!strncasecmp(option, "ClosedProvider",14))  { 
      strsep(&option,"=");
      cMultiOption *multi = new cMultiOption;
      multi->SetValue(UpString(option));
      closedProvider->Add(multi);
      }						      
    else {
      esyslog("AutoSort -WARNING-: Found unknown option '%s' in opengroups.conf", option);
      return false;
      }    
  return true;
}

void cOpenGroups::Dump(void){
  dsyslog("Dump OpenCa");
  openCa->Dump();
  dsyslog("Dump ClosedCa");
  closedCa->Dump();
  dsyslog("Dump OpenProvider");
  openProvider->Dump();
  dsyslog("Dump ClosedProvider");
  closedProvider->Dump();
}

// ---- cMultiList ---------------------------------------------------

cMultiList::cMultiList(void)
{
  //dsyslog("Constuctor cMultiList");
}

cMultiList::~cMultiList()
{
//  dsyslog("Destructor cMultiList");
  //Clear();
  cList<cMultiOption>::Clear(); // valgrind

}

void cMultiList::Dump(void){
for (cMultiOption *option = First(); option; option = Next(option)){
  dsyslog("cMultiList::Dump - '%s'", option->Value());
  }
}

void cMultiList::AddValue(char *newValue){
  cMultiOption *NewOption = new cMultiOption;
  NewOption->SetValue(strdup(newValue));
  Add(NewOption);
}

void cMultiList::AppendList(cMultiList *ListToAppend){
  for (cMultiOption *option = ListToAppend->First(); option; option = ListToAppend->Next(option)){
    AddValue(option->Value());
    }
}

char *cMultiList::DeleteDoubles(void){
  char *result = NULL;
  for (cMultiOption *option = First(); option; option = Next(option)){
    for (cMultiOption *option2 = Next(option); option2; option2 = Next(option2)){
      if (strcmp(option->Value(),option2->Value()) == 0 ) {
        char buf[64];
	if (result == NULL) {
	  snprintf(buf,sizeof(buf),"%s",option2->Value());
	} else {
	  snprintf(buf,sizeof(buf),"%s, %s", result, option2->Value());
	  }
        result = strdup(buf);
	Del(option2);
        }
      }
    }
  return result;
}


cMultiList *cMultiList::Copy(void){
  cMultiList *NewList = new cMultiList;
  for (cMultiOption *option = First(); option; option = Next(option)){
    NewList->AddValue(strdup(option->Value()));
    }
  return NewList;    
}

// ---- cMultiOption ---------------------------------------------------

cMultiOption::cMultiOption(void)
{  
//  dsyslog("Constuctor cMultiOption");
  value=NULL;
}

cMultiOption::~cMultiOption()
{
//  dsyslog("Destructor cMultiOption: '%s'", value);
  value=NULL;
  free(value);
}


