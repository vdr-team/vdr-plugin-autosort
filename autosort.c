/*
 * AutoSort.c: A plugin for the Video Disk Recorder
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id$
 */

#include <vdr/plugin.h>

static const char *VERSION        = "0.1.3";
static const char *DESCRIPTION    = "Channel AutoSort";
static const char *MAINMENUENTRY  = "AutoSort Restart";

class cPluginAutoSort : public cPlugin {
private:
  // Add any member variables or functions you may need here.
public:
  cPluginAutoSort(void);
  virtual ~cPluginAutoSort();
  virtual const char *Version(void) { return VERSION; }
  virtual const char *Description(void) { return DESCRIPTION; }
  virtual const char *CommandLineHelp(void);
  virtual bool ProcessArgs(int argc, char *argv[]);
  virtual bool Initialize(void);
  virtual bool Start(void);
  virtual void Stop(void);
  virtual void Housekeeping(void);
  virtual const char *MainMenuEntry(void);
  virtual cOsdObject *MainMenuAction(void);
  virtual cMenuSetupPage *SetupMenu(void);
  virtual bool SetupParse(const char *Name, const char *Value);
  virtual bool Service(const char *Id, void *Data = NULL);
  virtual const char **SVDRPHelpPages(void);
  virtual cString SVDRPCommand(const char *Command, const char *Option, int &ReplyCode);
  };

static cPluginAutoSort *PluginAutoSort = new cPluginAutoSort;

#include "autosort_menu.h"
#include "autosort_main_thread.h"

cPluginAutoSort::cPluginAutoSort(void)
{
  // Initialize any member variables here.
  // DON'T DO ANYTHING ELSE THAT MAY HAVE SIDE EFFECTS, REQUIRE GLOBAL
  // VDR OBJECTS TO EXIST OR PRODUCE ANY OUTPUT!
}

cPluginAutoSort::~cPluginAutoSort()
{
  // Clean up after yourself!
}

const char *cPluginAutoSort::MainMenuEntry(void)
{
  if (!hideMenu) 
    return MAINMENUENTRY;
   else
    return NULL; 
}

const char *cPluginAutoSort::CommandLineHelp(void)
{
  // Return a string that describes all known command line options.
  return NULL;
}

bool cPluginAutoSort::ProcessArgs(int argc, char *argv[])
{
  // Implement command line argument processing here if applicable.
  return true;
}

bool cPluginAutoSort::Initialize(void)
{
  // Initialize any background activities the plugin shall perform.
  return true;
}

bool cPluginAutoSort::Start(void)
{
  // Start any background activities the plugin shall perform.
  cAutoSortMainThread::Init();
  return true;
}

void cPluginAutoSort::Stop(void)
{
  // Stop any background activities the plugin shall perform.
  cAutoSortMainThread::Exit();
}

void cPluginAutoSort::Housekeeping(void)
{
  // Perform any cleanup or other regular tasks.
}

cOsdObject *cPluginAutoSort::MainMenuAction(void)
{
  // Perform the action when selected from the main VDR menu.
  // Reload config AND do complete Run
  cAutoSortMainThread::Restart();
  return NULL;
}

cMenuSetupPage *cPluginAutoSort::SetupMenu(void)
{
  // Return a setup menu in case the plugin supports one.
  return new cMenuAutoSortSetup;
}

bool cPluginAutoSort::SetupParse(const char *Name, const char *Value)
{
  // Parse your own setup parameters and store their values.
       if (!strcasecmp(Name, "MessageLevel"))     messageLevel      = atoi(Value);
  else if (!strcasecmp(Name, "MessageTime"))      messageTime       = atoi(Value);
  else if (!strcasecmp(Name, "SeperateNew"))      seperateNew       = atoi(Value);
  else if (!strcasecmp(Name, "SortNew"))          sortNew           = atoi(Value);
  else if (!strcasecmp(Name, "SortUnsorted"))     sortUnsorted      = atoi(Value);
  else if (!strcasecmp(Name, "HideMenu"))         hideMenu          = atoi(Value);
  else if (!strcasecmp(Name, "VersionSort"))      versionSorting    = atoi(Value);  
  else if (!strcasecmp(Name, "LastTimeStamp"))    lastTimeStampSet  = atoi(Value);  
  else if (!strcasecmp(Name, "SetTimeStamps"))    writeTimeStamp    = atoi(Value);  
  else if (!strcasecmp(Name, "TimeStampDelta"))   timeStampSetDelta = atoi(Value);  
  else if (!strcasecmp(Name, "UseFixedAuto"))     useFixedAuto      = atoi(Value);
  else if (!strcasecmp(Name, "UseFixedUnsorted")) useFixedUnsorted  = atoi(Value);
  else if (!strcasecmp(Name, "UseFixedNew"))      useFixedNew       = atoi(Value);
  else if (!strcasecmp(Name, "FixedAuto"))        fixedAuto         = atoi(Value);
  else if (!strcasecmp(Name, "FixedUnsorted"))    fixedUnsorted     = atoi(Value);
  else if (!strcasecmp(Name, "FixedNew"))         fixedNew          = atoi(Value);
  else if (!strcasecmp(Name, "StartTime"))        startTime         = atoi(Value);
  else if (!strcasecmp(Name, "LastDuration"))     lastDuration      = atoi(Value);
  else if (!strcasecmp(Name, "AverageDuration"))  averageDuration   = atoi(Value);
  else if (!strcasecmp(Name, "ActiveChannel"))    activeChannel     = atoi(Value);

  else
     return false;
  return true;
  
}

bool cPluginAutoSort::Service(const char *Id, void *Data)
{
  // Handle custom service requests from other plugins
  return false;
}

const char **cPluginAutoSort::SVDRPHelpPages(void)
{
  // Return help text for SVDRP commands this plugin implements
  return NULL;
}

//const char *cPluginAutoSort::SVDRPCommand(const char *Command, const char *Option, int &ReplyCode)
cString cPluginAutoSort::SVDRPCommand(const char *Command, const char *Option, int &ReplyCode)
{
  // Process SVDRP commands this plugin implements
  return NULL;
}

VDRPLUGINCREATOR(cPluginAutoSort); // Don't touch this!
