#ifndef __AUTOGROUPS_H
#define __AUTOGROUPS_H

#include "autogroup.h"
#include <vdr/config.h>


class cAutoGroups : public cConfig<cAutoGroup> {
private:
  void ReIndex(void);
public:
  cAutoGroups(void);
  virtual ~cAutoGroups();
  //~cAutoGroups();
  bool Load (const char *FileName, bool AllowComments, bool MustExist);
  cAutoGroup *GetFirstByOrder (void);
  cAutoGroup *GetNextByOrder (int OrdIdx);
  cAutoGroup *GetFirstByPrio (void);
  cAutoGroup *GetNextByPrio (int PrioIdx);
  cAutoGroup *GetByName (char *search);
  void Dump(void);
  };

extern cAutoGroups AutoGroups;

#endif
