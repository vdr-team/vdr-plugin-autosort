#ifndef __AUTOSORT_MAIN_THREAD_H
#define __AUTOSORT_MAIN_THREAD_H

#include <vdr/thread.h>
#include "opengroups.h"
#include "autogroup.h"
#include <vdr/plugin.h>

class cAutoSortMainThread: public cThread {
private:
        bool m_Active;
        static cAutoSortMainThread *m_Instance;
protected:
        virtual void Action(void);
        void Stop(void);
public:
        cAutoSortMainThread();
        virtual ~cAutoSortMainThread();
        static void Init(void);
        static void Restart(void);
        static void Exit(void);
	void CheckDelimiters(void);
	void PlaceChannel(cChannel *channel);
        void LoopChannels (void);
	cAutoGroup *InGroup(int ChIndex);
	void MoveGroupToEnd (int ChIndex);
	void MoveGroupBehindGroup (int ChIndex, cAutoGroup *group);
	void AddGroup (cAutoGroup *group);
	void InsGroup (cAutoGroup *group, int ChIndex);
	void DeleteGroup (int ChIndex);
	void Sort(cAutoGroup *group);
	void SetTimeStamp(int ChIndex);
	static void FlushNewChannels(void);
	void MoveChannelToGroup (int ChIndex, cAutoGroup *group);	
};

#endif 
