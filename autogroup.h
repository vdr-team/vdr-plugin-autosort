#ifndef __AUTOGROUP_H
#define __AUTOGROUP_H

#include <string.h>
#include "autosort_tools.h"
#include "opengroups.h"
#include <vdr/tools.h>

#define MAXAUTOOPTIONS 64 // the maximum number of multioptions

class cAutoGroup : public cListObject {
private:
  char *name;
  int priority;
  int pIdx;
  int order;
  int oIdx;
  int fixedPos;  
  char *startName;

  cMultiList *hasNameList;
  cMultiList *notNameList;
  cMultiList *hasProviderList;
  cMultiList *notProviderList;
  cMultiList *caValueList;
  cMultiList *notCaValueList;
  cMultiList *hasSourceList;
  cMultiList *audioLangList;
/* 
  struct t_hasName{
    char *hasName;
    };
  t_hasName hasNameList[MAXAUTOOPTIONS];
  struct t_notName{
    char *notName;
    };
  t_notName notNameList[MAXAUTOOPTIONS];
  struct t_hasSource{
    char *hasSource;
    };
  t_hasSource hasSourceList[MAXAUTOOPTIONS];
  struct t_hasProvider{
    char *hasProvider;
    };
  t_hasProvider hasProviderList[MAXAUTOOPTIONS];    
  struct t_notProvider{
    char *notProvider;
    };
  t_notProvider notProviderList[MAXAUTOOPTIONS];    
  struct t_caValue{
    char *caValue;
    };
  t_caValue caValueList[MAXAUTOOPTIONS];    
  struct t_notCaValue{
    char *notCaValue;
    };
  t_notCaValue notCaValueList[MAXAUTOOPTIONS];    
  struct t_audioLang{
    char *audioLang;
    };
  t_audioLang audioLangList[MAXAUTOOPTIONS];    
*/

  bool subSections;
  bool sort;
  bool keepHere;
  bool setCaFF;
  bool setCa0;
  bool saveDelete;
  bool realDelete;
  bool isSpecial;
  // Tri-states
  // -1 : must NOT have
  //  0 : don't care
  //  1 : must have
  int hasApid;
  int hasVpid;
  int hasTpid;
  int iCanSee;
  int hasCa;
  int hasOpenCa;
  int hasClosedCa;
  int hasOpenProvider;
  int hasClosedProvider;
  int timeStampDays;
public:
  cAutoGroup(void);
  virtual ~cAutoGroup();
  cAutoGroup *Copy(cAutoGroup *OldGroup);
  bool Parse(const char *s);
  bool ParseOption(char *option);
  void DeleteDoubles(void);
  void CopyOpenGroups(void);
  void Print(void);
  char *Name(void)                       { return name; };
  void SetName(char *Name)               { name = strdup(Name); };
  char *StartName(void)                  { return startName; };
  void SetStartName(char *StartName)     { startName = strdup(StartName); };
  int Priority (void)                    { return priority; };  
  void SetPriority (int Priority)        { priority = Priority; }; 
  int Order (void)                       { return order; };  
  int OIdx (void)                        { return oIdx; };  
  int PIdx (void)                        { return pIdx; };  
  bool KeepHere (void)                   { return keepHere; };
  bool Sort (void)                       { return sort; };
  bool SubSections (void)                { return subSections; };
  void SetSubSections (bool SubSections) { subSections = SubSections; };
  bool IsSpecial (void)                  { return isSpecial; };
  bool SetCaFF (void)                    { return setCaFF; };
  bool SetCa0 (void)                     { return setCa0; };
  bool SaveDelete (void)                 { return saveDelete; };
  bool RealDelete (void)                 { return realDelete; };
  void SetOrder (int Order)              { order = Order; };
  void SetFixedPos (int FPos)            { fixedPos = FPos; };
  int FixedPos (void)                    { return fixedPos; };
  void SetPIdx (int PIdx)                { pIdx = PIdx; }; 
  void SetOIdx (int OIdx)                { oIdx = OIdx; }; 
  void SetSort (void)                    { sort = true; }; 
  void SetSpecial (void)                 { isSpecial = true; };
  int HasApid (void)                     { return hasApid; };
  int HasVpid (void)                     { return hasVpid; };
  int HasTpid (void)                     { return hasTpid; };
  int HasCa (void)                       { return hasCa; };
  int ICanSee (void)                     { return iCanSee; };
  int HasOpenCa (void)                   { return hasOpenCa; };
  int HasClosedCa (void)                 { return hasClosedCa; };
  int HasOpenProvider (void)             { return hasOpenProvider; };
  int HasClosedProvider (void)           { return hasClosedProvider; };
  int TimeStampDays (void)               { return timeStampDays; };

  cMultiList *HasNameList (void)         { return hasNameList; };
  cMultiList *NotNameList (void)         { return notNameList; };
  cMultiList *HasSourceList (void)       { return hasSourceList; };
  cMultiList *HasProviderList (void)     { return hasProviderList; };
  cMultiList *NotProviderList (void)     { return notProviderList; };
  cMultiList *CaValueList (void)         { return caValueList; };
  cMultiList *NotCaValueList (void)      { return notCaValueList; };
  cMultiList *AudioLangList (void)       { return audioLangList; };

/*
  char *HasName (int NIdx)               { return hasNameList[NIdx].hasName; };
  char *NotName (int nNIdx)              { return notNameList[nNIdx].notName; };
  char *HasSource (int SIdx)             { return hasSourceList[SIdx].hasSource; };
  char *HasProvider (int PrIdx)          { return hasProviderList[PrIdx].hasProvider; };
  char *NotProvider (int PrIdx)          { return notProviderList[PrIdx].notProvider; };
  char *CaValue (int CaIdx)              { return caValueList[CaIdx].caValue; };
  char *NotCaValue (int nCaIdx)          { return notCaValueList[nCaIdx].notCaValue; };
  char *AudioLang (int AIdx)             { return audioLangList[AIdx].audioLang; };
*/  
  int GetChIndex(void);
  void ModifyChannelToGroup(int ChIndex, bool Out);
  bool ChannelFitsGroup(int ChIndex);
  };

#endif
