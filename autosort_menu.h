#ifndef __AUTOSORT_MENU_H
#define __AUTOSORT_MENU_H

#include <math.h>
#include <vdr/menu.h>

extern int maxMoves;
extern int messageLevel;
    // 0 - No Messages
    // 1 - LoopsReady
    // 2 - GroupReady
    // 3 - ChannelMove
extern int messageTime;
extern bool housekeepingRun;
extern bool versionSorting;
extern bool hideMenu;
extern int timeStampSetDelta;
    // 1 hour   = 3600
    // 1 day    = 86400
    // 1 week   = 604800
    // 1 month  = 2592000
extern bool writeTimeStamp;
extern int lastTimeStampSet;
extern bool writeNewTimeStamp;
extern bool saveSetup;
extern bool sortUnsorted;
extern bool sortNew;
extern bool seperateNew;
extern bool useFixedAuto;
extern bool useFixedUnsorted;
extern bool useFixedNew;
extern int fixedAuto;
extern int fixedUnsorted;
extern int fixedNew;
extern int startTime;
extern int lastDuration;
extern int averageDuration;
extern int activeChannel;

extern const char *ConfigDir;


/*
-- Setup
   -- Group AutoSort
   -- Group Unsorted
   -- Group NewChannels
   -- TimeStamps
   -- Others
        Hide MainMenu Entry
        Message Level
        Use 'version' Sort
        Run on StartUp
   -- Flush New Channels
   -- Reload Config
*/

class cASInfoItem : public cOsdItem {
public:
  cASInfoItem(const char *Text);
  };

class cMenuAutoSortSetup : public cMenuSetupPage {
private:
  int newHideMenu;
protected:
  virtual void Store(void);
public:
  cMenuAutoSortSetup(void);
  virtual eOSState ProcessKey(eKeys Key);
  };

class cMenuGroupAutoSort : public cMenuSetupPage {
private:
  int newUseFixedAuto;
  int newFixedAuto;
protected:
  virtual void Store(void);
public:
  cMenuGroupAutoSort(void);
  };

class cMenuGroupUnsorted : public cMenuSetupPage {
private:
  int newSortUnsorted;
  int newFixedUnsorted;
  int newUseFixedUnsorted;
protected:
  virtual void Store(void);
public:
  cMenuGroupUnsorted(void);
  };

class cMenuGroupNewChannels : public cMenuSetupPage {
private:
  int newSortNew;
  int newSeperateNew;
  int newUseFixedNew;
  int newFixedNew;
protected:
  virtual void Store(void);
public:
  cMenuGroupNewChannels(void);
  };

class cMenuTimeStamps : public cMenuSetupPage {
private:
  int newTimeStampSetDelta;
  int newWriteTimeStamp;
protected:
  virtual void Store(void);
public:
  cMenuTimeStamps(void);
  void StoreTime(void); 
  };

class cMenuOthers : public cMenuSetupPage {
private:
  int newMessageLevel;
  int newHideMenu;
  int newVersionSorting;
protected:
  virtual void Store(void);
public:
  cMenuOthers(void);
  };

#endif
