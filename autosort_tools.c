#include <stdio.h>
#include <ctype.h>
#include <vdr/plugin.h>
#include "autosort_menu.h"
#include "autosort_tools.h"
#include <sys/time.h>

char *UpString (char *ToUp) {
  int i = 0;
  char *Up = ToUp;  
  while (ToUp[i] != '\0'){
    Up[i] = toupper(ToUp[i]);
    i++;
    }  
  return Up;
}

char *KillSpecial(char *s)
{
  int i = 0;
  int j = 0;
  char *ks = s;  
  while (s[i] != '\0'){
    if ((!isspace(s[i])) && (!ispunct(s[i]))) {
      ks[j] = s[i];
      j++;
      }
    i++;
    } 
    ks[j] = '\0';
  return ks;
}

void ASMessage (int Level, const char *Text)
{
  char *namedText;
  asprintf(&namedText, "AutoSort: %s", Text);
  if  (messageLevel >= Level) {
  /*
    if (!Skins.IsOpen()){
      Skins.Message(mtInfo, namedText, Setup.OSDMessageTime);
      }
  */
    dsyslog("OSD: %s", namedText);
  } else {
    dsyslog("%s", namedText);
    }
  namedText=NULL;
  free(namedText);
}


