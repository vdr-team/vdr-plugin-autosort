#include "autosort_menu.h"
#include "autosort_main_thread.h"

int maxMoves = 20;
int messageLevel = 2;
// 0 - No Messages
// 1 - LoopsReady
// 2 - GroupReady
// 3 - ChannelMove
int messageTime = 5;
bool housekeepingRun = true;
bool versionSorting = true;
bool hideMenu = true;
int timeStampSetDelta = 604800;
// 1 hour   = 3600
// 1 day    = 86400
// 1 week   = 604800
// 1 month  = 2592000
bool writeTimeStamp = true;
int lastTimeStampSet = 0;
bool writeNewTimeStamp = false;
bool saveSetup = false;
bool sortUnsorted = true;
bool sortNew = false;
bool seperateNew = false;
bool useFixedAuto = false;
bool useFixedUnsorted = false;
bool useFixedNew = false;
int fixedAuto = 0;
int fixedUnsorted = 0;
int fixedNew = 0;

int startTime = 0;
int lastDuration = 0;
int averageDuration = 0;
int activeChannel = 0;

const char *ConfigDir =  cPlugin::ConfigDirectory();



cASInfoItem::cASInfoItem(const char *Text)
{
  SetSelectable(false);
  SetText(Text);
}

/*
cMenuAutoSortMain::cMenuAutoSortMain()
{
  Add(new cOsdItem("Stop",osUser9));
  Add(new cOsdItem("Reload Config",osUser9));
}
*/

cMenuAutoSortSetup::cMenuAutoSortSetup(void)
{
  Add(new cASInfoItem(" -- Settings ------------"));
  Add(new cOsdItem("Group 'AutoSort'",osUser1));
  Add(new cOsdItem("Group 'Unsorted'",osUser2));
  Add(new cOsdItem("Group 'New Channels'",osUser3));
  Add(new cOsdItem("Timestamps",osUser4));
  Add(new cOsdItem("Others",osUser5));
  Add(new cASInfoItem(" -- Actions -------------"));
  //Add(new cOsdItem("Delete TimeStamps",osUser7));
  Add(new cOsdItem("Flush 'New Channels'",osUser8));
  Add(new cOsdItem("Reload Config",osUser9));

  //Add(new cASInfoItem(""));
  char buf[32];
  
  if (activeChannel != 0) {
    snprintf(buf,sizeof(buf),"Now: %d %s", Channels.Get(activeChannel)->Number(), Channels.Get(activeChannel)->Name());
    Add(new cASInfoItem(buf));
    }

  if (startTime != 0) {
    snprintf(buf,sizeof(buf),"Start: %s", *TimeToString(startTime));
    Add(new cASInfoItem(buf));
    if (averageDuration != 0) {
      snprintf(buf,sizeof(buf),"End:  %s", *TimeToString(startTime + averageDuration));
      Add(new cASInfoItem(buf));
    } else {
      Add(new cASInfoItem("End:  Unknown"));
      }
  } else {
    Add(new cASInfoItem(" - Not running -"));  
    }
  
  if (lastDuration !=0) {
    snprintf(buf,sizeof(buf),"Last Duration:  %d min", (int)rint(lastDuration / 60));
    Add(new cASInfoItem(buf));
    }
  if (averageDuration !=0) {
    snprintf(buf,sizeof(buf),"Mean Duration:  %d min", (int)rint(averageDuration / 60));
    Add(new cASInfoItem(buf));
    }
//  free(buf); // valgrind complained about this

  CursorDown();
}

eOSState cMenuAutoSortSetup::ProcessKey(eKeys Key)
{
  eOSState state = cOsdMenu::ProcessKey(Key);
  switch(state) {
    case osUser1:
      return(AddSubMenu(new cMenuGroupAutoSort));
    case osUser2:
      return(AddSubMenu(new cMenuGroupUnsorted));
    case osUser3:
      return(AddSubMenu(new cMenuGroupNewChannels));
    case osUser4:
      return(AddSubMenu(new cMenuTimeStamps));
    case osUser5:
      return(AddSubMenu(new cMenuOthers));
    case osUser7:
      dsyslog("AutoSort: - NOT WORKING NOW - shall delete TimeStamps");
      state=osContinue;
      break;
    case osUser8:
      cAutoSortMainThread::FlushNewChannels();
      state=osBack;
      // FixMe: How to prevent Setup saving here?
      break;
    case osUser9:
      dsyslog("AutoSort: Reload and Restart");
      cAutoSortMainThread::Restart();
      state=osBack;
      // FixMe: How to prevent Setup saving here?
      break;

    case osUnknown:
      //if(Key==kOk) { Store(); state=osBack; }
      break;

    default:
      break;
    }
  return state;
}

void cMenuAutoSortSetup::Store(void)
{
}

cMenuGroupAutoSort::cMenuGroupAutoSort(void)
{
  SetSection("AutoSort - 'AutoSort'");
  newFixedAuto         = fixedAuto;
  newUseFixedAuto         = useFixedAuto;
  Add(new cMenuEditBoolItem( "Use FixedPos for 'Auto Sort'",    &newUseFixedAuto));
  Add(new cMenuEditIntItem ( "FixedPosition for 'Auto Sort'",   &newFixedAuto));
}

void cMenuGroupAutoSort::Store(void)
{
  cPluginManager::GetPlugin("autosort")->SetupStore("UseFixedAuto",    useFixedAuto      = newUseFixedAuto);
  cPluginManager::GetPlugin("autosort")->SetupStore("FixedAuto",       fixedAuto         = newFixedAuto);
}


cMenuGroupUnsorted::cMenuGroupUnsorted(void)
{
  SetSection("AutoSort - 'Unsorted'");
  newSortUnsorted      = sortUnsorted;
  newUseFixedUnsorted  = useFixedUnsorted;
  newFixedUnsorted     = fixedUnsorted;
  Add(new cMenuEditBoolItem( "Sort 'Unsorted' channels",        &newSortUnsorted));  
  Add(new cMenuEditBoolItem( "Use FixedPos for 'Unsorted'",     &newUseFixedUnsorted));  
  Add(new cMenuEditIntItem ( "FixedPosition for 'Unsorted'",    &newFixedUnsorted));
}

void cMenuGroupUnsorted::Store(void)
{
  cPluginManager::GetPlugin("autosort")->SetupStore("SortUnsorted",    sortUnsorted      = newSortUnsorted);
  cPluginManager::GetPlugin("autosort")->SetupStore("UseFixedUnsorted",useFixedUnsorted  = newUseFixedUnsorted);
  cPluginManager::GetPlugin("autosort")->SetupStore("FixedUnsorted",   fixedUnsorted     = newFixedUnsorted);
}


cMenuGroupNewChannels::cMenuGroupNewChannels(void)
{
  SetSection("AutoSort - 'New Channels'");
  newSeperateNew       = seperateNew;
  newSortNew           = sortNew;
  newUseFixedNew       = useFixedNew;
  newFixedNew          = fixedNew;
  Add(new cMenuEditBoolItem( "Keep new channels seperated",     &newSeperateNew));  
  Add(new cMenuEditBoolItem( "Sort 'New Channels'",             &newSortNew));  
  Add(new cMenuEditBoolItem( "Use FixedPos for 'New Channels'", &newUseFixedNew));  
  Add(new cMenuEditIntItem ( "FixedPosition for 'New Channels'",&newFixedNew));
}

void cMenuGroupNewChannels::Store(void)
{
  cPluginManager::GetPlugin("autosort")->SetupStore("SeperateNew",     seperateNew       = newSeperateNew);
  cPluginManager::GetPlugin("autosort")->SetupStore("SortNew",         sortNew           = newSortNew);
  cPluginManager::GetPlugin("autosort")->SetupStore("UseFixedNew",     useFixedNew       = newUseFixedNew);
  cPluginManager::GetPlugin("autosort")->SetupStore("FixedNew",        fixedNew          = newFixedNew);
}


cMenuTimeStamps::cMenuTimeStamps(void)
{
  SetSection("AutoSort - TimeStamps");
  newTimeStampSetDelta = (int)rint(timeStampSetDelta / 86400);
  newWriteTimeStamp    = writeTimeStamp;
  Add(new cMenuEditBoolItem( "Set TimeStamps",                  &newWriteTimeStamp));  
  Add(new cMenuEditIntItem ( "Days between setting TimeStamps", &newTimeStampSetDelta, 1));

  Add(new cASInfoItem(""));
  char buf[32];
  snprintf(buf,sizeof(buf),"Last: %s", *TimeToString(lastTimeStampSet));
  Add(new cASInfoItem(buf));
  snprintf(buf,sizeof(buf),"Next: %s", *TimeToString(timeStampSetDelta + lastTimeStampSet));
  Add(new cASInfoItem(buf));
  free(buf);
}

void cMenuTimeStamps::Store(void)
{
  cPluginManager::GetPlugin("autosort")->SetupStore("SetTimeStamps",   writeTimeStamp    = newWriteTimeStamp);
  cPluginManager::GetPlugin("autosort")->SetupStore("TimeStampDelta",  timeStampSetDelta = (newTimeStampSetDelta * 86400));
}

void cMenuTimeStamps::StoreTime(void)
{
  dsyslog("Autosort: Saving LastTimeStamp %d - %s", lastTimeStampSet, *TimeToString(lastTimeStampSet));
  cPluginManager::GetPlugin("autosort")->SetupStore("LastTimeStamp",lastTimeStampSet);
  Setup.Save();
}

cMenuOthers::cMenuOthers(void)
{
  SetSection("AutoSort - Others");
  newMessageLevel      = messageLevel;
  newHideMenu          = hideMenu;
  newVersionSorting    = versionSorting;
  Add(new cMenuEditBoolItem( "Hide MainMenu entry",             &newHideMenu));  
  Add(new cMenuEditIntItem ( "Message level",                   &newMessageLevel, 0, 3));
  Add(new cMenuEditBoolItem( "Use 'version' sort",              &newVersionSorting));  
}

void cMenuOthers::Store(void)
{
  cPluginManager::GetPlugin("autosort")->SetupStore("MessageLevel",    messageLevel      = newMessageLevel);
  cPluginManager::GetPlugin("autosort")->SetupStore("HideMenu",        hideMenu          = newHideMenu);
  cPluginManager::GetPlugin("autosort")->SetupStore("VersionSort",     versionSorting    = newVersionSorting);
}

